# 1*1 convolutional layers
It is very likely that the network will learn redundant/correlated feature maps for any layer, so it makes sense to do dimensionality reduction to get rid of this redundancy and reduce the number of parameters.  

If layer *a* has *m* channels, and its previous layer *b* has *n* channels, then the total amount of parameters of layer *a* will be *m\*n\*kernel_size*. If we add a 1*1 convolutional layer *c* with *k* channels in between, the number of parameters will become *m\*k\*kernel_size+k\*n*.

# "same" convolution
Letting the output retain the spatial resolution of the input allows for simply stack up more layers without affecting(decreasing) the spatial resolution. Moreover, with such spacial consistency we can add some operation between the input and output of a set of layers, as said in the residual network paper.

# locally connected/shared layers
The most common form of convolutional layers process two properties based on two assumptions:  
1) locally connected (assume that the value of a pixel does not affect the value of another pixel far away  
2) shared weights (assume that different parts of an image share some common patterns  

If the input are not arbitrary images, but have similar contents (e.g. faces), then in higher layers the second assumption does not hold. We may want to relax the shared weight constraint, say weights are shared locally instead of globally, or do not share at all.
# Nesterov momentum vs. classical momentum
Classical momentum:

v_t+1 = m\*v(t) - lr\*g(w_t)  
w_t+1 = w_t + v_t+1  

Nesterov momentum:


v_t+1 = m\*v(t) - lr\*g(w_t+m*v_t)  
w_t+1 = w_t + v_t+1  

The Nesterov method can be shown to be just the classical momentum with more weight to the *lr\*g* term, and less weight to the *v* term.

# AdaGrad
AdaGrad combined with momentum turns out to converge at least 25% faster than SGD/momentum/Nesterov/AdaGrad. Though it takes the most amount of memory (3*parameters).
# sigmoid + cross-entropy vs. hinge loss
The "spring model" verification loss used in DeepID2 does not seem very sophisticated. One may wonder why not use a sigmoid function to make the loss more noise-resistant. According to PRML 7.1.2 sigmoid+cross-entropy is not much different from hinge loss, which is not much different from the so called "spring model". Only the "spring model" does not use a margin.

# cubic learning rates
Sometimes we want to decrease the learning rate continuously across different magnitudes say from 1e-2 to 1e-4. However, if we simply decrease it linearly, the learning rate is going to decrease from 1e-2 to 1e-3 in the first 90% of the time, and from 1e-3 to 1e-4 only the last 10% of the time.  

So we need a function that decreases quickly at first and goes slowly towards the end, (1-x)^3 satisfies the needs it decreases to 0.125 at 0.5, and (0.125)^2 at 3/4 and keeps decreasing by 87.5% at every midpoint of the remaining half, because a^n b^n = (ab)^n .

# behavior of triplets
For sampling pairs for the verification loss, say 20 pairs for instance, a simple method would be randomly choose 10 positive pairs and 10 negative pairs, or alternatively we can sample 10 triplets (x,p,n), make a positive pair (x,p) and a negative pair (x,n).  

The later will result in a "slow but steady" curve, because the "pull" and "push" effects on positive and negative pairs of the same anchor tend to cancel out.
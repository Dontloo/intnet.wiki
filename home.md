Welcome to the IntNet, yet another deep learning library.  
[Structure Overview](../wikis/code_structure)  
[A Brief Tutorial](../wikis/tutorial)  
Some tricks and notes for [CNNs](../wikis/cnn_tricks), [optimization](../wikis/optimization_tricks), [implementation](../wikis/implementation_notes), [and others](../wikis/notes).
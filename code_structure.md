# neural networks  
A neural network typically defines two functions:  
1) a loss function **E(theta|x,y)**  
2) a output function **f(x|theta)**  

The output function **f(x|theta)** does not have to follow the forward pass of **E**, which means, the output network and can be completely different from the training network, as long as it uses (a subset of) the trained parameters **theta**.

In short, the training network defines a function of training **theta**, and the output network defines a function of using **theta**.  

So in our implementation the training and output functions are separated. Loss functions are implemented as classes, mainly for the sake of saving/loading trained parameters and plotting error curves.  While output functions are simply implemented as (wrapped) functions.

# layers
Layers are basic building blocks of networks, a layer defines one particular operation and parameters that may involve.  

Some kind of layers are expected to yield different behaviors at training and test time (e.g. the dropout layer). In our implementation, instead of letting it behave differently, we prefer changing the network structure for the output function, as it essentially is a different function. See [dropout layers](../wikis/implementation_notes#dropout-layer) for details.

# optimizer
An optimizer specifies a strategy for updating parameters usually based on the (sub)gradients with respect to some loss.  

Often we want the learning parameters to be able to change over time. So in each iteration we want to do three things:  
1) update the learning parameters according to the current training progress  
2) back propagation to get gradients  
2) update the parameters using the gradients and the updated learning parameters  

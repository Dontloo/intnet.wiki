# building a feed-forward neural network
Inputs should be wrapped in input layers.
```python
# input/target layer
l_x = intnet.layer.Input((None, channel, row, column), floatX)
l_y = intnet.layer.Input((None,), "int32")
```
Build a simple network by stacking up layers.
```python
# network
l_soft = intnet.layer.FullyConnected(l_x, "softmax", cate_num)
# loss
l_xent = intnet.layer.CrossEntropy(l_soft, l_y)
```
Define a feed-forward neural network, it should be aware of the loss (topmost) layer.
```python
# model
foonet = intnet.ffnn.ModelFFNN(l_xent)
```
Build a function for training, with inputs, outputs, and an optimizer.
```python
# training function
opt = intnet.com.Optimizer("sgd", intnet.com.AdaptParam('fixed', 0.00001))
train_fn = foonet.train_fn([l_x, l_y], l_xent, opt)
```
Build an output function.
```python
# output function
l_pred = intnet.layer.ArgMax(l_soft)
pred_fn = intnet.ffnn.output_fn(l_x, l_pred)
```
Initialize the parameters based some strategy or trained model. The parameters will be initialized/loaded layer-wise by a depth-first search starting from the topmost layer.
```python
# initialize
foonet.init_param("uniform_heuristic")
```
Mini-batch training.
```python
# train
training_steps = 1000
batch_sze = 32
for i, x, y in intnet.train.RandomBatchLauncher(training_steps, batch_sze, dat):
    loss = train_fn(x, y)
    print("Iter %d: Loss %g" % (i + 1, loss))
```
Inference.
```python
# predict
dat = intnet.train.CategoricalData(test_list, "lazy", floatX)
for i, x, y in intnet.train.SplitBatchLauncher(batch_sze, dat):
    print "prediction:", pred_fn(x)
    print "truth:", y
```
Serialize trained model, by a layer-wise depth-first search.
```python
# persistence
print "serializing training and test lists."
model_file = "foonet.pickle"
foonet.save_param(model_file)
```
For full executable script [/FusionFace/intnet/blob/master/intnet/examples/fooNet.py][1].

[1]: http://192.168.2.2/FusionFace/intnet/blob/master/intnet/examples/fooNet.py
# dropout layer
One specialty of dropout layers is, it has different behaviors in training and inference functions, which may bring us the need for specifying the output of a layer accordingly (instead of assigning a fixed output for training and inference).  

So we may want to specify the output on the fly (say via a function get_out() when building fucntions). The problem is, if we want more than one outputs for my functions (say y and z), and we call y.get_out() and z.get_out() separately to get the outputs, then the get_out() functions of corresponding-underlying-overlapping layers will get called twice, which means, if we use local Theano variables in the get_out() functions (which is very likely to be the case), then actually y and z will take two different computation paths in spite of the fact that y and z actually share the same intermediate results.  

The Lasagne way of solving this is, it runs topological sorting before generating the outputs, such that common intermediate results can be discovered. While Keras just prevents this from happening by only allow you to output the loss layer, good job.

Come to think about it, all we need is to tell the dropout layer that we want another way of computing the output. Hmmm, all in all training and inference are two different functions, instead of letting one operation generate different behaviors, we can just replace it with another function during compile time, so we break the Lasagne/Keras way of stacking layers and make the predecessors of a layer changeable, such that we can make use of the shared parameters in other functions.

# Theano matrix inversion
At the time of writing, Theano calls Numpy for matrix inversion, and Numpy does not run on the GPU (unless linked with cuBLAS), so don't think of using Theano to accelerate computation that involves matrix inversion.